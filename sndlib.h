
// SNDLIB.H - Header file //////////////////////////////////////////////////

// G L O B A L S  E X T E R N A L  D E C L A R A T I O N S////////////////////

extern unsigned ct_voice_status;

// P R O T O T Y P E S  /////////////////////////////////////////////////////

int Voc_Init_Driver();

void Voc_Terminate_Driver();

int Voc_Play_Sound(Mix_Chunk *chunk);

void Voc_Stop_Sound(void);

void Voc_Pause_Sound(void);

void Voc_Continue_Sound(void);

void Voc_Set_Status_Addr();

Mix_Chunk *Voc_Load_Sound(char *filename);

void Voc_Unload_Sound(Mix_Chunk *chunk);


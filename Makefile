TARGET = warlock
CFLAGS = -Wall `sdl-config --cflags`
LIBS = -lm -lSDL -lSDL_mixer
CC = gcc
SRC = graphics.c \
      scale2x.c \
      sndlib.c \
      warlock.c
OBJ = $(SRC:.c=.o)

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(OBJ) $(LIBS) -o $(TARGET)
	strip $(TARGET)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm *.o
	rm $(TARGET)

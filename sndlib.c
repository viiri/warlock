
// I N C L U D E S ///////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_mixer.h>
#include "sndlib.h"

// G L O B A L S  ////////////////////////////////////////////////////////////

unsigned ct_voice_status;

// F U N C T I O N S /////////////////////////////////////////////////////////

int Voc_Init_Driver(void)
{

int status=0;

status = SDL_Init(SDL_INIT_AUDIO);
status = Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 1, 1024);

// return status

printf("Driver Initialized\n");

return(status);

}

//////////////////////////////////////////////////////////////////////////////

void Voc_Terminate_Driver(void)
{

Mix_CloseAudio();

}

/////////////////////////////////////////////////////////////////////////////

int Voc_Play_Sound(Mix_Chunk *chunk)
{

ct_voice_status = 1;

Mix_PlayChannel(-1, chunk, 0);

return ct_voice_status;

}

/////////////////////////////////////////////////////////////////////////////

void Voc_Stop_Sound(void)
{

Mix_HaltChannel(-1);

}

/////////////////////////////////////////////////////////////////////////////

void Voc_Pause_Sound(void)
{

Mix_Pause(-1);

}

/////////////////////////////////////////////////////////////////////////////

void Voc_Continue_Sound(void)
{

Mix_Resume(-1);

}

/////////////////////////////////////////////////////////////////////////////

void _Voc_Update_Status(int channel)
{

ct_voice_status = 0;

}

/////////////////////////////////////////////////////////////////////////////

void Voc_Set_Status_Addr()
{

Mix_ChannelFinished(_Voc_Update_Status);

} // Voc_Set_Status_Addr

//////////////////////////////////////////////////////////////////////////////

Mix_Chunk *Voc_Load_Sound(char *filename)
{

// loads a sound off disk into memory and points a pointer to it

return Mix_LoadWAV(filename);

} // end Voc_Load_Sound

//////////////////////////////////////////////////////////////////////////////

void Voc_Unload_Sound(Mix_Chunk *chunk)
{

Mix_FreeChunk(chunk);

}

//////////////////////////////////////////////////////////////////////////////

